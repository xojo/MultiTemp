# MultiTemp

Created for the 2018 Xojo Just Code Challenge, this iOS app uses the OpenWeatherMap API to get temperature info for the cities you specify.

Sign up for a free account with OpenWeatherMap to get an APIKey: https://openweathermap.org/appid
