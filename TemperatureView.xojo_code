#tag IOSView
Begin iosView TemperatureView
   BackButtonTitle =   ""
   Compatibility   =   ""
   LargeTitleMode  =   "1"
   Left            =   0
   NavigationBarVisible=   True
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   "Temperatures"
   Top             =   0
   Begin iOSTable TempTable
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AllowRefresh    =   True
      AutoLayout      =   TempTable, 4, TempSelector, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   TempTable, 1, <Parent>, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   TempTable, 2, <Parent>, 2, False, +1.00, 4, 1, -0, , True
      AutoLayout      =   TempTable, 3, TopLayoutGuide, 4, False, +1.00, 4, 1, 0, , True
      EditingEnabled  =   False
      EstimatedRowHeight=   -1
      Format          =   "0"
      Height          =   386.0
      Left            =   0
      LockedInPosition=   False
      Scope           =   2
      SectionCount    =   0
      Top             =   65
      Visible         =   True
      Width           =   320.0
   End
   Begin Xojo.Net.HTTPSocket TempSocket
      LockedInPosition=   False
      Scope           =   0
   End
   Begin Xojo.Core.Timer CacheTimer
      Height          =   32
      Height          =   "32"
      Left            =   100
      Left            =   100
      LockedInPosition=   False
      Mode            =   "2"
      PanelIndex      =   -1
      Parent          =   ""
      Period          =   600000
      Scope           =   2
      Tolerance       =   0
      Top             =   100
      Top             =   100
      Width           =   32
      Width           =   "32"
   End
   Begin iOSSegmentedControl TempSelector
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   TempSelector, 4, BottomLayoutGuide, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   TempSelector, 8, , 0, True, +1.00, 4, 1, 29, , True
      AutoLayout      =   TempSelector, 1, TempTable, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   TempSelector, 2, TempTable, 2, False, +1.00, 4, 1, 0, , True
      Caption         =   ""
      Enabled         =   True
      Height          =   29.0
      Left            =   0
      LockedInPosition=   False
      Scope           =   2
      Segments        =   "℃\n\nTrue\r℉\n\nFalse"
      Top             =   451
      Value           =   0
      Visible         =   True
      Width           =   320.0
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Event
		Sub Open()
		  GetWeather
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function CelsiusToFahrenheit(temp As Double) As Double
		  // [°F] = [°C] × 9/5 + 32
		  
		  Return temp * (9 / 5) + 32
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function FahrenheightToCelsius(temp As Double) As Double
		  // [°C] = ([°F] - 32) × 5/9
		  
		  Return (temp - 32.0) * (5 / 9)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub GetWeather()
		  // https://openweathermap.org
		  // You will need to sign up for your own free API key.
		  // Then set the kAPIKey constant to it.
		  
		  // City IDs are better for lookup. You can get these from the openweathermap
		  // site.
		  
		  // South Portland: 4979244
		  // Austin: 4099974
		  // Munich: 2867714
		  
		  Cities = Array("4979244", "4099974", "2867714")
		  
		  Dim owmURL As Text = "https://api.openweathermap.org/data/2.5/group?id=%cityIDs%&units=%units%&APPID=%APIKey%"
		  owmURL = owmURL.Replace("%cityIDs%", Text.Join(Cities, ","))
		  
		  If TempSelector.Value = 0 Then
		    APIUnits = "metric"
		  Else
		    APIUnits = "imperial"
		  End If
		  owmURL = owmURL.Replace("%units%", APIUnits)
		  owmURL = owmURL.Replace("%APIKey%", kAPIKey)
		  
		  TempSocket.Send("GET", owmURL)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Load()
		  If UseCache Then
		    LoadTable(JSONCache)
		    TempTable.EndRefresh
		  Else
		    GetWeather
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub LoadTable(json As Text)
		  TempTable.RemoveAll
		  TempTable.AddSection("")
		  
		  Dim jsonDict As Xojo.Core.Dictionary
		  jsonDict = Xojo.Data.ParseJSON(json)
		  
		  Dim cities() As Auto = jsonDict.Value("list")
		  For Each c As Xojo.Core.Dictionary In cities
		    Dim city As Text = c.Value("name")
		    Dim mainDict As Xojo.Core.Dictionary = c.Value("main")
		    Dim temp As Double = mainDict.Value("temp")
		    
		    Dim units As Text = TempSelector.Item(TempSelector.Value).Title
		    
		    If (units = "℃" And APIUnits = "metric") Or _
		      (units = "" And APIUnits = "imperial") Then
		      // temp is correct
		    Else
		      // convert temp
		      If units = "℃" Then
		        temp = FahrenheightToCelsius(temp)
		      Else
		        temp = CelsiusToFahrenheit(temp)
		      End If
		    End If
		    
		    TempTable.AddRow(0, city + " is currently " + temp.ToText(Xojo.Core.Locale.Current, "#.0") + units)
		  Next
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private APIUnits As Text
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Cities() As Text
	#tag EndProperty

	#tag Property, Flags = &h21
		Private JSONCache As Text
	#tag EndProperty

	#tag Property, Flags = &h21
		Private UseCache As Boolean
	#tag EndProperty


	#tag Constant, Name = kAPIKey, Type = Text, Dynamic = False, Default = \"ReplaceWithYourAPIKey", Scope = Private
	#tag EndConstant


#tag EndWindowCode

#tag Events TempTable
	#tag Event
		Sub Refreshed()
		  Load
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TempSocket
	#tag Event
		Sub PageReceived(URL as Text, HTTPStatus as Integer, Content as xojo.Core.MemoryBlock)
		  // Restart Timer so it invalidates the cache after 10 minutes
		  CacheTimer.Mode = Xojo.Core.Timer.Modes.Off
		  CacheTimer.Mode = Xojo.Core.Timer.Modes.Multiple
		  
		  UseCache = True
		  
		  // Process returned JSON and add cities, temps, weather
		  Dim jsonText As Text
		  jsonText = Xojo.Core.TextEncoding.UTF8.ConvertDataToText(content)
		  JSONCache = jsonText
		  
		  LoadTable(jsonText)
		  
		  TempTable.EndRefresh
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events CacheTimer
	#tag Event
		Sub Action()
		  // Do not call the weather API more than once every 10 minutes
		  // This timer repeats every 10 minutes to tell the app that no longer
		  // has to use the cache and instead can send an API request.
		  UseCache = False
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TempSelector
	#tag Event
		Sub ValueChanged()
		  Load
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackButtonTitle"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NavigationBarVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIcon"
		Group="Behavior"
		Type="iOSImage"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabTitle"
		Group="Behavior"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LargeTitleMode"
		Visible=true
		Group="Behavior"
		InitialValue="2"
		Type="LargeTitleDisplayModes"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
#tag EndViewBehavior
